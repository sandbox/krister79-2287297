<div id="instagram-panel" class="instagram-block">
    <div id="instagram-image-wrapper">
    <?php $max = variable_get('instagram_limit'); ?>
    <?php foreach($rows->data as $index => $row): ?>
    <?php print theme('instagram-image', array('row' => $row)); ?>
    <?php endforeach; ?>
    <?php while($index++ < $max): ?>
    <div class="instagram-image">
    <img src="<?php print drupal_get_path('module', 'dinstagram').'/default-75x75.jpg'; ?>" />
    </div>
    <?php endwhile; ?>
    </div>
</div>
